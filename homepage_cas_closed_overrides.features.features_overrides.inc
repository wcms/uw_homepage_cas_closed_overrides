<?php

/**
 * @file
 * homepage_cas_closed_overrides.features.features_overrides.inc
 */

/**
 * Implements hook_features_override_default_overrides().
 */
function homepage_cas_closed_overrides_features_override_default_overrides() {
  // This code is only used for UI in features. Exported alters hooks do the magic.
  $overrides = array();

  // Exported overrides for: variable.
  $overrides["variable.cas_logout_destination.value"] = 'user';

  return $overrides;
}
