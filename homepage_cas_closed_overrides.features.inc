<?php

/**
 * @file
 * homepage_cas_closed_overrides.features.inc
 */

/**
 * Implements hook_strongarm_alter().
 */
function homepage_cas_closed_overrides_strongarm_alter(&$data) {
  if (isset($data['cas_logout_destination'])) {
    $data['cas_logout_destination']->value = 'user'; /* WAS: '' */
  }
}
